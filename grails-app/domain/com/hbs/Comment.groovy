package com.hbs

class Comment {

    Integer id
    String comment
    Date dateCreated

    static belongsTo = [owner: User, hotel: Hotel, room: Room]

    static constraints = {
        hotel(nullable: true)
        room(nullable: true)
        owner(blank: false)
        comment(blank: false, size: 5..100)
    }
}
