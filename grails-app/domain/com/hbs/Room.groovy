package com.hbs

class Room {

    Integer id
    Integer number
    String description
    String type
    Integer price
    Integer occupancy

    static belongsTo = [hotel : Hotel]

    static hasMany = [comments: Comment, booking: Booking]

    static constraints = {
        number(blank: false)
        description(blank: false, maxSize: 1000)
        type(inList: ["LUX", "HALF LUX", "STANDARD", "ECONOMY"], blank: false)
    }
}
