package com.hbs

class User {

    Integer id
    String email
    String password
    String firstName
    String lastName
    String phone
    String role

    static hasMany = [hotels : Hotel, comments: Comment, bookings: Booking]

    static constraints = {
        email(unique: true, email: true, blank: false, size: 5..30)
        password(blank: false, size: 32..32)
        firstName(blank: false, minSize: 2)
        lastName(blank: false, minSize: 2)
        phone(nullable: true, size: 6..13)
        role(inList: ["ROLE_USER", "ROLE_HOTEL", "ROLE_ADMIN"], blank: false)
    }

    String toString() {
        return "${firstName} ${lastName}"
    }
}
