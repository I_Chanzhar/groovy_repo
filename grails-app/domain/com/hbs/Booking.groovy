package com.hbs

class Booking {

    Integer id
    Date dateIn
    Date dateOut
    String status

    static belongsTo = [owner: User, room: Room, hotel: Hotel, user: AnonymousUser]

    static hasOne = [payment: Payment]

    static constraints = {
        dateIn(nullable: false, validator: { date, booking ->
            date < booking.dateOut })
        dateOut(nullable: false, validator: { date, booking ->
            date > booking.dateIn })
        status(inList: ["CONFIRMED", "NOT_CONFIRMED"], blank: false)
        owner(nullable: true, validator: { own, booking ->
            (own == null) != (booking.user == null)
        })
        user(nullable: true, validator: { usr, booking ->
            (usr == null) != (booking.owner == null)
        })
        room(nullable: false)
        hotel(nullable: false)
        payment(nullable: true)
    }
}
