package com.hbs

class Payment {

    Integer id
    String payment
    Integer cost
    static belongsTo = [booking: Booking]

    static constraints = {
        payment(blank: false)
        cost(nullable: false)
    }

    String toString() {
        return "${payment} ${cost}"
    }

}
