package com.hbs

class Hotel {

    Integer id
    String title
    String description
    String city
    String country

    static belongsTo = [owner: User]

    static hasMany = [rooms: Room, comments: Comment, bokings: Booking]

    static constraints = {
        title(blank: false)
        description(blank: false, maxSize: 1000)
        country(blank: false)
        city(blank: false)
        owner(nullable: false)
    }
}
