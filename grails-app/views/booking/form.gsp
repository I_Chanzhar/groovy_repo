<html>
<head>
    <meta name="layout" content="main" />
    <title>Reservation form</title>
</head>
<body>
<g:if test="${session.user}">
<g:form controller="booking" action="addBooking" class="form-horizontal">
    <div class="control-group">
        <label class="control-label">Hotel</label>
        <div class="control-label">
            ${room.hotel?.title}
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Room</label>
        <div class="control-label">
            Number:${room.number}<br> Type:${room.type} <br> Price:${room.price}
        </div>
    </div>
    <g:hiddenField name="booking.room.id" value="${room.id}" />
    <g:hiddenField name="booking.hotel.id" value="${room.hotel.id}" />
    <g:hiddenField name="booking.status" value="NOT_CONFIRMED" />
    <g:hiddenField name="room" value="${room}" />
    <div class="control-group">
        <label class="control-label" for="inputDateIn">Date In</label>
        <div class="controls">
            <g:datePicker name="booking.dateIn" id="inputDateIn" placeholder="DateIn" precision="day" relativeYears="[0..1]" value="${booking?.dateIn}"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputDateOut">Date Out</label>
        <div class="controls">
            <g:datePicker name="booking.dateOut" id="inputDateOut" placeholder="DateOut" precision="day" relativeYears="[0..1]" value="${booking?.dateOut}"/>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <g:submitButton class="btn btn-large" name="save" value="Save" />
        </div>
    </div>

</g:form>
</g:if>
<g:else>
    <g:form controller="booking" action="addBookingAnonymous" class="form-horizontal">
        <div class="control-group">
            <label class="control-label">Hotel</label>
            <div class="control-label">
                ${room.hotel?.title}
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Room</label>
            <div class="control-label">
                Number:${room.number}<br> Type:${room.type} <br> Price:${room.price}
            </div>
        </div>
        <g:hiddenField name="booking.room.id" value="${room.id}" />
        <g:hiddenField name="booking.hotel.id" value="${room.hotel.id}" />
        <g:hiddenField name="booking.status" value="NOT_CONFIRMED" />
        <g:hiddenField name="room" value="${room}" />
        <div class="control-group">
            <label class="control-label" for="inputDateIn">Date In</label>
            <div class="controls">
                <g:datePicker name="booking.dateIn" id="inputDateIn" placeholder="DateIn" precision="day" relativeYears="[0..1]" value="${booking?.dateIn}"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputDateOut">Date Out</label>
            <div class="controls">
                <g:datePicker name="booking.dateOut" id="inputDateOut" placeholder="DateOut" precision="day" relativeYears="[0..1]" value="${booking?.dateOut}" style="width:auto;"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="firstName">First Name</label>
            <div class="controls">
                <g:textField name="user.firstName" id="firstName" placeholder="Email"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="lastName">Last Name</label>
            <div class="controls">
                <g:textField name="user.lastName" id="lastName" placeholder="Email"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputEmail">Email</label>
            <div class="controls">
                <g:textField name="user.email" id="inputEmail" placeholder="Email"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPhone">Phone Number</label>
            <div class="controls">
                <g:textField name="user.phone" id="inputPhone" placeholder="Email"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <g:submitButton class="btn btn-large" name="save" value="Save" />
            </div>
        </div>

    </g:form>
</g:else>
</body>
</html>