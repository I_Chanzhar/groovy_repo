<html>
<head>
    <meta name="layout" content="main"/>
    <title>My Bookings</title>
</head>

<body>
<h1 style="text-align: center">Bookings</h1>
<table class="table table-striped">
    <tr>
        <th>Hotel</th>
        <th>Room</th>
        <th>Date In</th>
        <th>Date Out</th>
        <th>Payment</th>
        <th>Status</th>
    </tr>
    <g:each in="${bookings}" var="booking">
        <tr>
            <td>${booking.hotel.title}</td>
            <td>${booking.room.number}</td>
            <td><g:formatDate format="dd/MM/yyyy" date="${booking.dateIn}"/></td>
            <td><g:formatDate format="dd/MM/yyyy" date="${booking.dateOut}"/></td>
            <td>${booking.payment}</td>
            <td>${booking.status}</td>
        </tr>
    </g:each>
</table>

</body>
</html>