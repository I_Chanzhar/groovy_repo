<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main"/>
    <title>Manage Bookings</title>
</head>

<body>
<h1 style="text-align: center">Manage bookins <br> ${hotel.title}</h1>
<table class="table table-striped">
    <tr>
        <th>Room</th>
        <th>Date In</th>
        <th>Date Out</th>
        <th>Payment</th>
        <th>Contacts</th>
        <th>Status</th>
        <th></th>
    </tr>
<tr>
    <g:each in="${bookings}" var="booking">
        <tr>
            <td>${booking.room.number}</td>
            <td><g:formatDate format="dd/MM/yyyy" date="${booking.dateIn}"/></td>
            <td><g:formatDate format="dd/MM/yyyy" date="${booking.dateOut}"/></td>
            <td>${booking.payment}</td>
            <td>
                <g:if test="${booking.owner}">
                    ${booking.owner.email}<br>${booking.owner.phone}
                </g:if>
                <g:else>
                    ${booking.user.email}<br>${booking.user.phone}
                </g:else>
            </td>
            <td>${booking.status}</td>
            <td>
                <div class="btn-group">
                    <g:if test="${booking.status == "NOT_CONFIRMED"}">
                        <g:link controller="booking" action="setStatus('CONFIRMED')" id="${booking.id}"
                                class="btn btn-success" style="width:55px;">Confirm</g:link>
                    </g:if>
                    <g:if test="${booking.status == "CONFIRMED"}">
                        <g:link controller="booking" action="invalidate" id="${booking.id}" class="btn btn-warning"
                                style="width:55px;">Invalidate</g:link>
                    </g:if>
                    <g:link controller="booking" action="delete" id="${booking.id}" class="btn btn-danger"
                            style="width:55px;">Delete</g:link>
                </div>
            </td>
        </tr>
    </g:each>
</tr>
</table>

</table>
</body>
</html>