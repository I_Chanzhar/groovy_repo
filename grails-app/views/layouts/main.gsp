<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Hotel Booking Service"/></title>
    <link href="${resource(dir: 'images', file: 'favicon.ico')}" rel="shortcut icon"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}"/>
    <g:javascript src="bootstrap.js"/>
    <g:layoutHead/>
    <r:layoutResources/>
</head>

<body>

<div class="container-fluid">
    <div class="row-fluid" style="text-align:center;">
        <div id="header" class="span10 offset1"><h1>HOTEL BOOKING SERVICE</h1></div>
    </div>

    <div class="span3 offset1 btn-group-wrap">
        <div id="menu" class="btn-group-vertical">
            <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink uri='/' />'" >General</button>
            <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink controller="hotel" action="listHotels"/>'">Hotels</button>
            <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink controller="room" action="findRoomsForm"/>'">Find-a-Room</button>
            <g:if test="${session.user}">
                <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink controller="booking" action="myBookings"/>'">My Bookings</button>
            </g:if>
            <g:if test="${session.user?.role == "ROLE_HOTEL" || session.user?.role == "ROLE_ADMIN"}">
                <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink controller="hotel" action="myHotels"/>'">My Hotels</button>
            </g:if>
            <g:if test="${session.user?.role == "ROLE_ADMIN"}">
                <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink controller="user" action="list"/>'">List Users</button>
            </g:if>
            <g:if test="${!session.user}">
                <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink controller="user" action="login"/>'" >Login</button>
                <button class="btn btn-large btn-info" onclick="location.href = '<g:createLink controller="user" action="reg_form"/>'">Register</button>
            </g:if>
            <g:if test="${session.user}">
                <button class="btn btn-large btn-info"
                        onclick="location.href = '<g:createLink controller="user" action="logout"/>'">Logout</button>
            </g:if>

        </div>
        <div style="text-align: center; font-weight: bold; width: 200px;">
            <g:if test="${session.user}">
                Logged in as, ${session.user}.
            </g:if>
        </div>
    </div>

    <div id="column-center" class="span10">
        <g:if test="${flash.message}">
        <div class="alert alert-info">
        <g:message code="${flash.message}"/>
        </div>
        </g:if>
        <g:layoutBody/>
    </div>

    <div class="row">
        <div class="span12" id="footer" style="text-align: center;"></div>

    </div>
</div>

<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
<g:javascript library="application"/>
<r:layoutResources/>
</body>
</html>
