<html>
<head>
    <meta name="layout" content="main"/>
    <title>registration</title>
</head>

<body>
<g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
</g:if>
<g:form action="registration">
    <table id="reg">
        <tr>
            <td>E-mail</td>
            <td><input type="text" name="email" value="${user?.email}"></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td>First Name</td>
            <td><input type="text" name="firstName"  value="${user?.firstName}"></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><input type="text" name="lastName"  value="${user?.lastName}"></td>
        </tr>
        <tr>
            <td>Phone</td>
            <td><input type="text" name="phone" value="${user?.phone}"></td>
        </tr>
        <tr>
            <td>Role</td>
            <td><g:radioGroup name="role"
                              labels="['HOTEL', 'COSTUMER']"
                              values="['ROLE_HOTEL', 'ROLE_USER']" value="ROLE_USER">
                ${it.radio} ${it.label}&nbsp;
            </g:radioGroup></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"><g:submitButton name="register" value="Register" /></td>
        </tr>
    </table>

</g:form>
</body>
</html>                            S