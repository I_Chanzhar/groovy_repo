<html>
<head>
    <title>Login</title>
    <meta name="layout" content="main"/>
    <style>
    #form {
        width: auto;
    }

    #form td {
        text-align: left;
        width: auto;
    }
    </style>
</head>

<body>

<g:if test="${flash.message}">
    <div class="message">${flash.message}</div>
</g:if>

<g:if test="${!session.user}">
    <div id="login">
        <g:form action="authentication">
            <table id="form">
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email"></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" name="password"></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center"><g:submitButton name="login" value="Login"/></td>
                </tr>
            </table>
        </g:form>
    </div>
</g:if>

<g:if test="${session.user}">
    <g:form action="logout">
        <g:submitButton name="logout" value="Logout"/>
    </g:form>
</g:if>
</body>
</html>