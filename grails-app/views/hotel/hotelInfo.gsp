<html>
<head>
    <meta name="layout" content="main"/>
    <title>HotelInfo</title>
</head>

<body>
    <table class="table table-bordered">
        <tr>
            <td colspan="2" style="text-align: center"><h1>${hotel?.title}</h1></td>
        </tr>
        <tr>
            <td><g:country code="${hotel?.country}"/></td>
            <td>${hotel?.city}</td>
        </tr>
        <tr>
            <td colspan="2">${hotel?.description}</td>
        </tr>
    </table>
</body>
</html>