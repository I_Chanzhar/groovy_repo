<html>
<head>
    <meta name="layout" content="main"/>
    <title>My Hotels</title>
</head>

<body>
<h1 style="text-align: center">My Hotels</h1>
<table class="table table-striped">
    <tr>
        <th>ID</th>
        <th>Hotel Name</th>
        <th>Country</th>
        <th>City</th>
        <th>Actions</th>
    </tr>
    <g:each in="${hotels}" var="hotel">
        <tr>
            <td>${hotel.id}</td>
            <td>${hotel.title}</td>
            <td><g:country code="${hotel.country}"/></td>
            <td>${hotel.city}</td>
            <td><div class="btn-group"><g:link controller="hotel" action="hotelInfo" id="${hotel.id}" class='btn'>HotelInfo</g:link>
            <g:link controller="room" action="hotelRooms" id="${hotel.id}" class='btn'>Rooms</g:link>
            <g:link controller="booking" action="manageBookings" id="${hotel.id}" class="btn">Manage Bookings</g:link>
            <g:link controller="hotel" action="delete" id="${hotel.id}" class="btn btn-danger">Delete</g:link></div></td>
        </tr>
    </g:each>
</table>
<div align="center">
<g:link controller="hotel" action="addHotelform" class="btn btn-success">Add Hotel</g:link>
</div>
</body>
</html>