<head>
    <meta name="layout" content="main"/>
  <title>Add hotel</title>
    <style>
        .control-label {
            text-align: right;
        }
    </style>
</head>
<body>
    <g:form action="addHotel" class="form-horizontal">
        <div class="control-group">
            <label class="control-label" for="inputTitle">Hotel Name</label>
            <div class="controls">
                <g:textField name="hotel.title" id="inputTitle" placeholder="Title"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputCountry">Country</label>
            <div class="controls">
                <g:countrySelect id="inputCountry" name="hotel.country" value="${country}"
                                 noSelection="['':'-Choose your country-']"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputCity">City</label>
            <div class="controls">
                <g:textField name="hotel.city" id="inputCity" placeholder="City"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputDescription">Description</label>
            <div class="controls">
                <g:textArea id="inputDescription" placeholder="Description" name="hotel.description" cols="20" rows="10"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <g:submitButton class="btn btn-large" name="save" value="Save" />
            </div>
        </div>

    </g:form>
</body>
</html>