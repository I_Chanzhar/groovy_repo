<html>
<head>
    <meta name="layout" content="main"/>
    <title>Hotels</title>
</head>

<body>
<table class="table table-striped">
    <tr>
        <th>Hotel Name</th>
        <th>Country</th>
        <th>City</th>
        <th></th>
    </tr>
    <g:each in="${hotels}" var="hotel">
        <tr>
            <td>${hotel.title}</td>
            <td><g:country code="${hotel.country}"/></td>
            <td>${hotel.city}</td>
            <td><div class="btn-group"><g:link controller="hotel" action="hotelInfo" id="${hotel.id}"  class='btn'>Hotel Info</g:link>
            <g:link controller="room" action="hotelRooms" id="${hotel.id}"  class='btn'>Rooms</g:link></div></td>
        </tr>
    </g:each>

</table>
</body>
</html>