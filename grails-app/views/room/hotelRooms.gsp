<html>
<head>
    <meta name="layout" content="main"/>
    <title>Hotel Rooms</title>
</head>

<body>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<table class="table table-striped">
    <tr>
        <th>Number</th>
        <th>Type</th>
        <th>Occupancy</th>
        <th>Price</th>
        <th colspan="3"></th>
    </tr>
    <g:each in="${rooms}" var="room">
        <tr>
            <td>${room.number}</td>
            <td>${room.type}</td>
            <td>${room.occupancy}</td>
            <td>${room.price}</td>
            <td><g:link controller="room" action="roomInfo" id="${room.id}">RoomInfo</g:link></td>
            <g:if test="${session.user?.id == hotel.owner.id}">
                <td><g:link controller="room" action="delete" id="${room.id}">Delete</g:link></td>
            </g:if>
            <td><g:link controller="booking" action="form" id="${room.id}" class="btn btn-success">Reserve</g:link></td>
        </tr>
    </g:each>
</table>
<g:if test="${session.user?.id == hotel.owner.id}">
    <div align="center">
        <g:link controller="room" action="room_form" id="${hotel.id}" class="btn btn-success">Add Room</g:link>
    </div>
</g:if>
</body>
</html>