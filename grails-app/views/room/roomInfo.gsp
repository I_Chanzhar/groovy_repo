<html>
<head>
    <meta name="layout" content="main"/>
    <title>Room Info</title>
</head>

<body>
<table class="table table-bordered">
    <tr>
        <td colspan="2" style="text-align: center"><h1>${room.hotel?.title}'s Room</h1></td>
    </tr>
    <tr>
        <td>Number: ${room.number}</td>
        <td>Occupancy: ${room.occupancy}</td>
    </tr>
    <tr>
        <td>Type: ${room.type}</td>
        <td>Price: ${room.price}</td>
    </tr>
    <tr>
        <td colspan="2">${room.description}</td>
    </tr>
</table>
<div align="center">
    <g:link controller="booking" action="form" id="${room.id}" class="btn btn-success">Reserve</g:link>
</div>
</body>
</html>