<html>
<head>
    <meta name="layout" content="main" />
    <title>Find-a-room</title>
</head>
<body>
<g:form action="findRooms">
    <div class="control-group">
        <label class="control-label" for="inputCountry">Country</label>
        <div class="controls">
            <g:countrySelect id="inputCountry" name="country" noSelection="['':'-Choose country-']"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputCity">City</label>
        <div class="controls">
            <g:textField name="city" id="inputCity" placeholder="City"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputType">Room Type</label>
        <div class="controls">
            <g:select id="inputType" name="type" from="${['LUX', 'HALF LUX', 'ECONOMY', 'STANDARD']}"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputOccupancy">Occupancy</label>
        <div class="controls">
            <g:field type="number" step="1" min="1" value="1" name="occupancy" id="inputOccupancy" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPrice">Price</label>
        <div class="controls">
            <g:field type="number" name="price" id="inputPrice" placeholder="Price"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputDateIn">Date In</label>
        <div class="controls">
            <g:datePicker name="dateIn" id="inputDateIn" placeholder="DateIn" precision="day" relativeYears="[0..1]"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputDateOut">Date Out</label>
        <div class="controls">
            <g:datePicker name="dateOut" id="inputDateOut" placeholder="DateOut" precision="day" relativeYears="[0..1]"/>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <g:submitButton class="btn btn-large" name="search" value="Search" />
        </div>
    </div>
</g:form>
</body>
</html>