<html>
<head>
  <meta name="layout" content="main" />
  <title>Result</title>
</head>
<body>

<g:if test="${size}">
    <h3>Results: ${size}</h3>
</g:if>
<g:else>
    <h3>No Results</h3>
</g:else>
<g:if test="${rooms}">
    <table class="table table-striped">
        <tr>
            <th>Hotel</th>
            <th>Number</th>
            <th>Type</th>
            <th>Occupancy</th>
            <th>Price</th>
            <th></th>
        </tr>
    <g:each in="${rooms}" var="room">
        <tr>
            <td>${room.hotel.title}</td>
            <td>${room.number}</td>
            <td>${room.type}</td>
            <td>${room.occupancy}</td>
            <td>${room.price}</td>
            <td>
                <div class="btn-group">
                    <g:link controller="room" action="roomInfo" id="${room.id}" class="btn btn-info">RoomInfo</g:link>
                    <g:link controller="booking" action="form" id="${room.id}" class="btn btn-success">Reserve</g:link>
                </div>
            </td>
        </tr>
    </g:each>
    </table>
</g:if>
</body>
</html>