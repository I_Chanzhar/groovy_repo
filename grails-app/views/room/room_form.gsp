<head>
    <meta name="layout" content="main"/>
    <title>Add room</title>
 </head>
<body>
<g:form action="addRoom" class="form-horizontal">
    <div class="control-group">
        <label class="control-label" for="inputNumber">Room Number</label>
        <div class="controls">
            <g:textField name="room.number" id="inputNumber" placeholder="Number"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputType">Type</label>
        <div class="controls">
            <g:select id="inputType" name="room.type" from="${['LUX', 'HALF LUX', 'ECONOMY', 'STANDARD']}"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputOccupancy">Occupancy</label>
        <div class="controls">
            <g:textField name="room.occupancy" id="inputOccupancy" placeholder="Occupancy"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputPrice">Price</label>
        <div class="controls">
            <g:textField name="room.price" id="inputPrice" placeholder="Price"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputDescription">Description</label>
        <div class="controls">
            <g:textArea id="inputDescription" placeholder="Description" name="room.description" cols="20" rows="10"/>
        </div>
    </div>
    <g:hiddenField name="room.hotel.id" value="${params.id}" />
    <div class="control-group">
        <div class="controls">
            <g:submitButton class="btn btn-large" name="save" value="Save" />
        </div>
    </div>

</g:form>
</body>
</html>