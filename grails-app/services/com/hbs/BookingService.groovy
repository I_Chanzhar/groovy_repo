package com.hbs

class BookingService {

    //Checking room availability on some dates for booking
    def checkBooking(Booking booking) {
        def c = Booking.createCriteria()
        def result = c {
            eq("room.id", booking.room.id)
            or {
                between("dateIn", booking.dateIn, booking.dateOut)
                between("dateOut", booking.dateIn, booking.dateOut)
                and {
                    le("dateIn", booking.dateIn)
                    ge("dateOut", booking.dateOut)
                }
            }
            maxResults(1)
        }

        return result.size()
    }

    //getting available rooms by filter
    def findRooms(Booking booking, Room room, Hotel hotel) {

        def rc = Room.createCriteria()
        def rooms = rc {
            if (room.occupancy)
                ge("occupancy", room.occupancy)
            if (room.type)
                eq("type", room.type)
            if (room.price)
                le("price", room.price)
            createAlias("hotel", "h")
            if (hotel.country) {
                eq("h.country", hotel.country)
                if (hotel.city)
                    eq("h.city", hotel.city)
            }
            order("price", "desc")
            maxResults(100)
        }

        def bc = Booking.createCriteria()
        def bookings = bc {
            or {
                between("dateIn", booking.dateIn, booking.dateOut)
                between("dateOut", booking.dateIn, booking.dateOut)
                and {
                    le("dateIn", booking.dateIn)
                    ge("dateOut", booking.dateOut)
                }
            }
        }

        rooms.removeAll(bookings*.room)
        return rooms
    }

    //Changing status entity booking - CONFIRMED/NOT_CONFIRMED
    def changeStatus(Booking booking, String status) {
        booking.status = status
        booking.save()
    }
}
