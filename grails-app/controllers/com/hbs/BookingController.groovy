package com.hbs

class BookingController {

    def bookingService

    def form() {
        def room = Room.findById(params.id)
        [room: room]
    }

    def addBooking() {
        def booking = new Booking(params.booking)
        booking.owner = session.user
        if (!bookingService.checkBooking(booking)) {
            if (booking.save()) {
                redirect(controller: 'booking', action: 'myBookings')
            } else {
                redirect(uri: '/')
            }
        } else {
            redirect(controller: "room", action: "hotelRooms", id: booking.hotel.id)
        }
    }

    def addBookingAnonymous() {
        def booking = new Booking(params.booking)
        def user = new AnonymousUser(params.user)
        if (!bookingService.checkBooking(booking)) {
            if (user.save()) {
                booking.user = user
                if (booking.save()) {
                    flash.message = "Successful, your room reservation saved"
                    redirect(uri: '/')
                } else {
                    flash.message = "Something wrong, your room reservation not saved"
                    redirect(uri: '/')
                }
            } else {
                flash.message = "wrong contact parameters"
                redirect(action: 'form', id: params.booking.room.id)
            }

        } else {
            flash.message = "that room already reserved, please select another"
            redirect(controller: "room", action: "hotelRooms", id: booking.hotel.id)
        }
    }

    def myBookings() {
        if (session.user){
            def bookings = Booking.findAllByOwner(session.user)
            [bookings: bookings]
        }
    }

    def manageBookings() {
        def hotel = Hotel.findById(params.id)
        def bookings = Booking.findAllByHotel(hotel)
        [bookings: bookings, hotel: hotel]
    }

    def delete() {
        def booking = Booking.findById(params.id)
        if (booking && session.user) {
            if (booking.owner == session.user || booking.hotel.owner == session.user || session.user.role == 'ROLE_ADMIN') {
                booking.delete()
                redirect(action: 'manageBookings', id: booking.hotel.id)
            } else {
                redirect(uri: '/')
            }
        } else {
            redirect(uri: '/')
        }
    }

    def confirm() {
        def booking = Booking.findById(params.id)
        if (bookingService.changeStatus(booking, "CONFIRMED")) {
            redirect(action: "manageBookings", id: booking.hotel.id)
        } else {
            redirect(uri: '/')
        }
    }

    def invalidate() {
        def booking = Booking.findById(params.id)
        if (bookingService.changeStatus(booking, "NOT_CONFIRMED")) {
            redirect(action: "manageBookings", id: booking.hotel.id)
        } else {
            redirect(uri: '/')
        }
    }
}