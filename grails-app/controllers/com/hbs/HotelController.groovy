package com.hbs

class HotelController {

    def addHotelform() {}

    def listHotels = {
        def hotels = Hotel.findAll()
        [hotels: hotels]
    }

    def myHotels = {
        if (session.user) {
            def hotels = Hotel.findAllByOwner(session.user)
            [hotels: hotels]
        } else {
            redirect(uri: '/')
        }
    }
    def hotelInfo = {
        def hotel = Hotel.findById(params.id)
        if (hotel) {
            [hotel: hotel]
        } else {
            redirect(uri: '/')
        }
    }

    def addHotel = {
        if (session.user?.role == 'ROLE_HOTEL' || session.user?.role == 'ROLE_ADMIN') {
            def hotel = new Hotel(params.hotel)
            hotel.owner = session.user
            if (hotel.save()) {
                redirect(action: 'myHotels')
            } else {
                redirect(uri: '/')
            }
        } else {
            redirect(uri: '/')
        }
    }

    def delete = {
        def hotel = Hotel.findById(params.id)
        if (hotel && session.user) {
            if (hotel.owner == session.user || session.user.role == 'ROLE_ADMIN') {
                hotel.delete()
                redirect(action: 'myHotels')
            } else {
                redirect(uri: '/')
            }
        } else {
            redirect(uri: '/')
        }

    }
}
