package com.hbs

class UserController {

    static scaffold = User

    def index() {}

    def login() {}

    def reg_form() {}

    def registration() {
        def user = new User(params)
        user.password = params.password.encodeAsMD5().toString()
        if (user.save()) {
            flash.message = "succesful"
            redirect(uri: '/')
        } else {
            flash.message = "something wrong"
            [user: user]
            redirect(action: "reg_form")
        }
    }

    def authentication = {
        def user = User.findByEmailAndPassword(params.email, params.password.encodeAsMD5().toString())
        if (user) {
            user.password = null
            session.user = user
            redirect(uri: '/')
        } else {
            flash.message = "Wrong email or password"
            redirect(action: "login")
        }
    }

    def logout = {
        session.user = null
        redirect(uri: '/')
    }
}
