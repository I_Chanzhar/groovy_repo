package com.hbs

class RoomController {

    def bookingService

    def room_form() {}

    def findRoomsForm() {}

    def roomInfo() {
        def room = Room.findById(params.id)
        if (room) {
            [room: room]
        } else {
            redirect(uri: '/')
        }
    }

    def hotelRooms = {
        def hotel = Hotel.findById(params.id)
        def rooms = Room.findAllByHotel(hotel)

        [rooms: rooms, hotel: hotel]
    }

    def addRoom = {
        def room = new Room(params.room)
        if (room.save()) {
            redirect(action: 'hotelRooms', id: room.hotel.id)
        } else {
            redirect(uri: '/')
        }
    }

    def delete = {
        def room = Room.findById(params.id)
        if (room && session.user) {
            if (room.hotel?.owner == session.user || session.user?.role == 'ROLE_ADMIN') {
                room.delete()
                redirect(action: 'hotelRooms', id: room.hotel.id)
            } else {
                redirect(uri: '/')
            }
        } else {
            redirect(uri: '/')
        }

    }

    def findRooms = {RoomSearchCommand rsc ->
        if (rsc.hasErrors()){
            redirect(action: "findRoomsForm")
        } else {
        def booking = new Booking(rsc.properties)
        def room = new Room(rsc.properties)
        def hotel = new Hotel(rsc.properties)
        def results = bookingService.findRooms(booking, room, hotel)
        def size = results.size()
        [rooms: results, size: size]
        }
    }


}

class RoomSearchCommand {
    String country
    String city

    String type
    Integer occupancy
    Integer price

    Date dateIn
    Date dateOut

    static constraints = {
        city(blank: true, nullable: true)
        country(blank: true, nullable: true)
        type(blank: true, nullable: true)
        occupancy(blank: true, nullable: true)
        price(blank: true, nullable: true)
        dateIn(nullable: false, validator: { date, booking ->
            date < booking.dateOut })
        dateOut(nullable: false, validator: { date, booking ->
            date > booking.dateIn })
    }
}